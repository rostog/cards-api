import { describe, beforeEach, it } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';
import authMiddleware, { encodeMessage, usedNonces } from '../lib/middleware/auth.middleware';


describe('Auth middleware', () => {
  const fakeData = {
    validPair: {
      api_key: '123456',
      api_sign: 'ssHEBbn9HJIDjnnBgcwEi7fVUivsQuuieghADMgzyuKCVyTGlwqS6RzQwSLBQtuBUIH2LLm6JnWKeeiyrgkQ0Q==',
    },
    invalidPair: {
      api_key: '654321',
      api_sign: 'rWkZ6ow1Q6CPdxyvY69GDqFq7tnRJJdplI348GSiLCXilSQ9CgGewiYW79T9QH2xH80VQVjDkzEbH5NBjDE+hg==',
    },
  };

  const fakeRequest = {
    headers: {
      nonce: 1487071721748000,
    },
    originalUrl: '/api/test',
    postData: {},
  };

  let nextSpy;

  beforeEach(() => {
    nextSpy = sinon.spy();
    delete usedNonces[fakeData.validPair.api_key];
  });

  // Does not provide api_key and api_sign
  it('should return "Unathorized" error', (done) => {
    authMiddleware(fakeRequest, {}, nextSpy);

    expect(nextSpy.calledWith(sinon.match.object)).to.be.true;
    expect(nextSpy.firstCall.args[0].message).to.be.equal('Unathorized');

    done();
  });

  // Provides wrong api_key and api_sign pair
  it('should return "Failed to authenticate, wrong credintials" error', (done) => {
    const { headers } = fakeRequest;

    headers.api_key = fakeData.invalidPair.api_key;
    headers.api_sign = fakeData.invalidPair.api_sign;

    authMiddleware(fakeRequest, {}, nextSpy);

    expect(nextSpy.calledWith(sinon.match.object)).to.be.true;
    expect(nextSpy.firstCall.args[0].message).to.be.equal('Failed to authenticate, wrong credintials');
    done();
  });

  // Provides all credintials properly
  it('should successfully authenticate', (done) => {
    const { headers } = fakeRequest;
    headers.api_key = fakeData.validPair.api_key;
    headers.api_sign = fakeData.validPair.api_sign;

    authMiddleware(fakeRequest, {}, nextSpy);

    expect(nextSpy.firstCall.args.length).to.be.equal(0);
    done();
  });

  // Tries to call with same none
  it('should return "wrong nonce" error', (done) => {
    const { headers } = fakeRequest;
    headers.api_key = fakeData.validPair.api_key;
    headers.api_sign = fakeData.validPair.api_sign;

    authMiddleware(fakeRequest, {}, nextSpy);
    authMiddleware(fakeRequest, {}, nextSpy);

    expect(nextSpy.firstCall.args.length).to.be.equal(0);
    expect(nextSpy.secondCall.args[0].message).to.be.equal('wrong nonce');
    done();
  });
});
