import crypto from 'crypto';
import querystring from 'querystring';


const apiKey = '123456';
const apiSecret = 'secret';
export const usedNonces = {};

export function encodeMessage(path, data, nonce, secret) {
  const message = querystring.stringify(data);
  const decSecret = new Buffer(secret, 'base64');
  const hash = new crypto.createHash('sha256');
  const hmac = new crypto.createHmac('sha512', decSecret);

  const hashDigest = hash.update(nonce + message).digest('binary');
  const hmacDigest = hmac.update(path + hashDigest, 'binary').digest('base64');

  return hmacDigest;
}


function authMiddleware(req, res, next) {
  const providedApiKey = req.headers.api_key;
  const providedApiSign = req.headers.api_sign;
  const nonce = req.headers.nonce;

  if (providedApiKey && providedApiSign && nonce) {
    if (usedNonces[apiKey] && usedNonces[apiKey] >= nonce) return next({ success: false, message: 'wrong nonce' });
    usedNonces[apiKey] = nonce;
    const generatedApiSign = encodeMessage(req.originalUrl, req.body, nonce, apiSecret);
    if (providedApiKey === apiKey && providedApiSign === generatedApiSign) {
      next();
    } else {
      next({ success: false, status: 401, message: 'Failed to authenticate, wrong credintials' });
    }
  } else {
    next({ success: false, status: 401, message: 'Unathorized' });
  }
}


export default authMiddleware;
