import express from 'express';
import cardCtrl from '../controllers/card.controller';

const router = express.Router();

router.post('/', cardCtrl.add);

router.route('/:cardNumber')
  .get(cardCtrl.get)
  .delete(cardCtrl.remove);

export default router;
