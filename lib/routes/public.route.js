import express from 'express';
import cardController from '../controllers/card.controller';


const router = express.Router();

router.get('/', cardController.getAll);

export default router;
