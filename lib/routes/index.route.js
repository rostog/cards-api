import express from 'express';
import publicRouter from './public.route';
import privateRouter from './private.route';
import authMiddleware from '../middleware/auth.middleware';

const router = express.Router();


router.use(publicRouter);

router.use(authMiddleware);

router.use(privateRouter);


export default router;
