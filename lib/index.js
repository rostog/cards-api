import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import router from './routes/index.route';
import './db';

const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


const port = process.env.PORT || 8080;
app.use(morgan('dev'));


app.use('/api/cards', router);


// Not Found Middleware
app.use('*', (req, res, next) => {
  res.status(404).json({ message: 'Route not found' });
});

// Basic Error Handling Middleware
app.use((err, req, res, next) => {
  res.status(err.status || 500).json({ success: false, message: err.message });
});

app.listen(port);

console.log(`card-api is listening on ${port}`);
