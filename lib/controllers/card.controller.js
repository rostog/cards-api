import Card from '../models/card.model';

// Public
function getAll(req, res, next) {
  Card.list()
    .then((result) => {
      res.json({
        sucess: true,
        message: 'Successfully retrieved all cards',
        result,
      });
    })
    .catch(err => next(err));
}


// Private
function add(req, res, next) {
  const { cardNumber, name, phone } = req.body;

  if (!(cardNumber && name && phone)) return next({ status: 400, message: 'Please provide all required inputs' });

  const card = new Card(cardNumber, name, phone);

  card.create()
    .then(() => res.json({
      sucess: true,
      message: 'Card Added',
    }))
    .catch(err => next(err));
}

function get(req, res, next) {
  const card = new Card(req.params.cardNumber);

  card.read()
    .then((result) => {
      if (!result) return next({ status: 404, message: 'Card number not found' });

      res.json({
        sucess: true,
        message: 'Successfully retrieved card',
        result,
      });
    })
    .catch(err => next(err));
}

function remove(req, res, next) {
  const card = new Card(req.params.cardNumber);

  card.remove()
    .then((changes) => {
      if (!changes) return next({ status: 404, message: 'Card number not found' });

      res.json({
        sucess: true,
        message: 'Successfully deleted card',
      });
    })
    .catch(err => next(err));
}

export default { getAll, add, get, remove };
