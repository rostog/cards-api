import faker from 'faker';
import db from '../db';

const table = 'cards';

class Card {
  constructor(cardNumber, name, phone) {
    this.cardNumber = cardNumber;
    this.name = name;
    this.phone = phone;
  }
  static list() {
    return new Promise((resolve, reject) => {
      db.connection
        .all(`SELECT cardNumber, name FROM ${table}`, (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        });
    });
  }
  create() {
    // Generate Fake address
    const address = faker.finance.bitcoinAddress();
    // Prepare Statement
    const stm = db.connection.prepare(`INSERT INTO ${table} (cardNumber, address, name, phone ) VALUES (?, ?, ?, ?)`);

    return new Promise((resolve, reject) => {
      stm.run(this.cardNumber, address, this.name, this.phone,
          (err) => {
            if (err) {
              reject(err);
            } else {
              resolve();
              stm.finalize();
            }
          });
    });
  }
  read() {
    return new Promise((resolve, reject) => {
      db.connection
        .get(`SELECT * FROM ${table} WHERE cardNumber = ?`, this.cardNumber,
          (err, row) => {
            if (err) {
              reject(err);
            } else {
              resolve(row);
            }
          });
    });
  }
  remove() {
    return new Promise((resolve, reject) => {
      db.connection
        .run(`DELETE FROM ${table} WHERE cardNumber = ?`, this.cardNumber,
          function removeCB(err) {
            if (err) {
              reject(err);
            } else {
              resolve(this.changes);
            }
          });
    });
  }

}

export default Card;
