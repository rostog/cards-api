import sqlite3 from 'sqlite3';
import _ from 'underscore';
import faker from 'faker';

const sqlite3Verb = sqlite3.verbose();

const connection = new sqlite3Verb.Database(':memory:');

connection.serialize(() => {
  connection.run('CREATE TABLE cards ( cardNumber TEXT NOT NULL UNIQUE, address TEXT NOT NULL UNIQUE, name TEXT NOT NULL, phone TEXT NOT NULL)');

  const stmt = connection.prepare('INSERT INTO cards (cardNumber, address, name, phone ) VALUES (?, ?, ?, ?)');

  // Fake Data
  _.each(_.range(20), () => {
    const randomNumber = faker.random.number;
    stmt.run(`${randomNumber({ min: 1000, max: 9999 })}-${randomNumber({ min: 1000, max: 9999 })}-${randomNumber({ min: 1000, max: 9999 })}-${randomNumber({ min: 1000, max: 9999 })}`,
      faker.finance.bitcoinAddress(), faker.name.findName(), faker.phone.phoneNumber());
  });


  stmt.finalize();

  connection.each('SELECT * FROM cards', (err, row) => {
    console.log(`${row.cardNumber}, ${row.name}, ${row.phone}`);
  });
});


export default { connection };
