# Cards API

api for storing cards information.

## Setup

in order to run the project you have to install NodeJS and npm.

Run ``npm install`` in your terminal to download all dependencies.

Project uses self-contained sqlite3 in-memory database.

## Run

to run project use command ``npm start``.

On start it generates 20 fake entries into database.
